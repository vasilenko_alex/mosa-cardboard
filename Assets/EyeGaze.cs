﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class EyeGaze : MonoBehaviour {
	
	public bool beenLooked;
	Component halo;
	bool toggleHalo;
	
	//
	void Awake() {    
		SetGazedAt(false);
		beenLooked = false;
		
		GameObject child = transform.GetChild (0).gameObject;
		halo = child.GetComponent ("Halo");
		halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
		toggleHalo = false;
	}
	
	void Update(){
	//	transform.Rotate (40f * Time.deltaTime, 100f * Time.deltaTime, 40f * Time.deltaTime);
		
		if (!beenLooked && !toggleHalo) {
			halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
			toggleHalo = true;
		}
	}
	
	public void SetGazedAt(bool gazedAt) {
		//		GetComponent<Renderer>().material.color = gazedAt ? Color.cyan : Color.white;
		beenLooked = gazedAt ? true : false;
		
		if (beenLooked) {
			halo.GetType ().GetProperty ("enabled").SetValue (halo, true, null);
			toggleHalo = false;
		}
		//		else
		//			halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
		
	}
	
}
