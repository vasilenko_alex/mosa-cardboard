﻿

 var walk : AudioClip;
     var run : AudioClip;
      
     var isWalking : boolean = false;
     var isRunning : boolean = false;
      
      function Start(){
     Cursor.visible = false;
      
      }
      
     function Update()
     {
     GetState();
     PlayAudio();
     }
      
      
     function GetState()
     {
     if ( Input.GetAxis( "Horizontal" ) || Input.GetAxis( "Vertical" ) )
     {
     if ( Input.GetKey( "left shift" ) || Input.GetKey( "right shift" ) )
     {
     // Running
     isWalking = false;
     isRunning = true;
     }
     else
     {
     // Walking
     isWalking = true;
     isRunning = false;
     }
     }
     else
     {
     // Stopped
     isWalking = false;
     isRunning = false;
     }
     }
      
      
     function PlayAudio()
     {
     if ( isWalking )
     {
     if ( GetComponent.<AudioSource>().clip != walk )
     {
     GetComponent.<AudioSource>().Stop();
     GetComponent.<AudioSource>().clip = walk;
     }
      
     if ( !GetComponent.<AudioSource>().isPlaying )
     {
     GetComponent.<AudioSource>().Play();
     }
     }
     else if ( isRunning )
     {
     if ( GetComponent.<AudioSource>().clip != run )
     {
     GetComponent.<AudioSource>().Stop();
     GetComponent.<AudioSource>().clip = run;
     }
      
     if ( !GetComponent.<AudioSource>().isPlaying )
     {
     GetComponent.<AudioSource>().Play();
     }
     }
     else
     {
     GetComponent.<AudioSource>().Stop();
     }
     }


