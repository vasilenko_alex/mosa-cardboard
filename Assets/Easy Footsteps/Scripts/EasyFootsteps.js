@script RequireComponent(CharacterController); //Easy Footsteps will not work as intended without a character controller
@script RequireComponent(AudioSource); //How are you going to play the sounds, eh?

//I believe that it is not hard to expand this script to suit your needs even more, well that´s why it is called
//Easy Footsteps. The point is, you shouldn´t even touch the Footstep function, just the update, and the update
//is extremely simple.


private var Controller : CharacterController;

private var FootstepHit : RaycastHit;
private var RayDown : Vector3;

private var Leg : int = 0; //The leg which is now moving, used to alternate between footstep sounds.

private var Loop1 : int = 0; //A sort of workaround :(
private var Loop2 : int = 0; //A sort of workaround to my mediocore programming knowledge :/
private var Loop3 : int = 0; //A sort of workaround, these loops just don´t want to cooperate :@
private var CheckLoop : int = 0;	//Another workaround for the startup checks.... ya don´t want ya console flooded with the mighty

//                    #   #           ##                                              #   #          
// ##  # #  #   #  ### ###  #  ### ### ### ##  ### ### ### # # ### ### ### ###     ### ##  
// # # # #  #   #  #   ##  ### ##  #   ##  # # #   ##  ##   #  #   ##  # #  #   #  # # # # 
// # # ###  ##  ## #   ###  #  ### #   ### # # ### ### ### # # ### ### ###  ##  ## ### # # 
//                         ##                                          #                     

								



var Speed : float = 2; //Insert the Character Controller´s movement speed, so the footsteps work at a correct rate.
					   //2 is a good starting value
					   //You can write a script that controls this value directly to the movement speed of the controller.
					   
var StepLength : float = 1; //How long do you want the step be, this controls the rate the footsteps are playing at.
							//1 is a good starting value
							
var LegLength : float = 1.5;	//How long should the tag detection raycast should be, 1.5 is a good value
								//As it lets on surfaces a little below also play the correct sound.
								//10 - on the other hand, is way too much - I recommend experimenting with this value.
							
var FirstLeg : Transform;	//Used by the Dual Leg Mode, determines the transform of legs, the transform should be parented
var SecondLeg : Transform;	//to the transform of the CharacterController gameobject, and should move with it.
							//The Leg1 and Leg2 transform should not move by itself.
							//An empty gameobject is the perfect fit.
							//These gameobjects are a workaround to a rather difficult raycasting math.

private var CurStepLength : float = 0; //This value is controlled by the Speed. How long to next footstep?

var DefaultLeg1FootStep : AudioClip; //Used when untagged.
var DefaultLeg2FootStep : AudioClip; //Used when untagged.

var UseTerrainFootsteps : boolean = true;
var TerrainFootsteps : AudioClip[];

var UseTagDependantSounds : boolean = false;
var UseDualLegMode : boolean = false;

var Leg1Footsteps : AudioClip[];	//Footstep sounds used for the first leg.
var Leg2Footsteps : AudioClip[];	//Footstep sounds used for the second leg.
var FootstepTags : String[];		//Tags used for the corresponding footstep!

//WARNING! When using multiple footstep sounds (Leg1Footsteps, Leg2Footsteps, FootstepTags) match them in order.
//If you have 2 different tags with different sounds make sure that there are matched all in the same place.
//Invalid example!

//Leg 1Footsteps v
	//mat1foot1.ogg
	//mat2foot1.ogg
//Leg 2Footsteps v
	//mat2foot2.ogg
	//mat1foot2.ogg	
//Footstep Tags v
	//Material 2
	//Material 1
	
//What´s wrong is that mat2 needs to be in the same place everywhere, not scarreted around, like in this example above.
//This is a valid configuration.

//Leg 1Footsteps v
	//mat1foot1.ogg
	//mat2foot1.ogg
//Leg 2Footsteps v
	//mat1foot2.ogg
	//mat2foot2.ogg	
//Footstep Tags v
	//Material 1
	//Material 2

function Start () {
	Controller = GetComponent(CharacterController); //Cache the CharacterController for quicker reference.
	RayDown = transform.TransformDirection (Vector3.down); //Cache the down direction, for quicker raycasting.
	StartupCheck();
}

function Update () {
	//If CurStepLength is bigger than StepLength then call Footstep
	if(CurStepLength >= StepLength) {
		Footstep();
	}
	//If the player moves Horizontal or Vertical axis and the controller is grounded, play the footsteps!
	if((Input.GetAxis("Horizontal") || Input.GetAxis("Vertical"))) {
		CurStepLength += Speed * Time.deltaTime;
	}
}

function StartupCheck () {
	//i think the messages are self explanitory.
	if(UseDualLegMode == true) {
		if(UseTagDependantSounds == false) {
			Debug.LogWarning("Dual Leg Mode is on but Tag Dependant Sounds is off. Dual Leg Mode won´t do anything. DISABLING.");
			UseDualLegMode = false;
		}
	}
	if(StepLength <= 0) {
		Debug.LogWarning("Variable StepLength is smaller or equal to zero - Easy Footsteps will not work correctly");
	}
	if(Controller == null) {
		//Debug.LogError("Easy Footsteps can not find the CharacterController. Easy Footsteps will NOT WORK!");
	}
	//if the dual legs mode is enabled, check if legs are assigned
	if(UseDualLegMode == true) {
		if(FirstLeg == null) {
			Debug.LogError("Dual Leg Mode is enabled but one of leg transforms are not assigned. This can cause a nullreferenceexception");
			UseDualLegMode = false;
			Debug.LogError("Dual Leg Mode automatically disabled, check log why!");
		}
		if(SecondLeg == null) {
			Debug.LogError("Dual Leg Mode is enabled but one of leg transforms are not assigned. This can cause a nullreferenceexception");
			UseDualLegMode = false;
			Debug.LogError("Dual Leg Mode automatically disabled, check log why!");
		}
	}
	if(DefaultLeg1FootStep == null) {
		Debug.LogError("One of the default footsteps are not assigned, Easy Footsteps will not work correctly!");
	}
	if(DefaultLeg2FootStep == null) {
		Debug.LogError("One of the default footsteps are not assigned, Easy Footsteps will not work correctly!");
	}
	//check if there aren´t any excess tags?
	if(FootstepTags.Length > Leg1Footsteps.Length) {
		Debug.LogError("There are more FootstepTags than Leg1Footsteps!");
		Debug.LogError("Disabling Tag Based Footsteps and Dual Leg Mode, check log why!");
		UseTagDependantSounds = false;
		UseDualLegMode = false;
	}
	if(FootstepTags.Length > Leg2Footsteps.Length) {
		Debug.LogError("There are more FootstepTags than Leg2Footsteps!");
		Debug.LogError("Disabling Tag Based Footsteps and Dual Leg Mode, check log why!");
		UseTagDependantSounds = false;
		UseDualLegMode = false;
	}
	//check for missing footsteps in the TagDependantFootstepMode
	if(UseTagDependantSounds == true) {
		for (var TestTag in FootstepTags) {
			if(Leg1Footsteps[CheckLoop] == null) {
				Debug.LogError("Missing footstep sound for " + TestTag + " in Leg 1 footsteps, Element " + CheckLoop);
			}
			if(Leg2Footsteps[CheckLoop] == null) {
				Debug.LogError("Missing footstep sound for " + TestTag + " in Leg 2 footsteps, Element " + CheckLoop);
			}
			if(TestTag == "") {
				Debug.LogError("Easy Footsteps - Tag name empty, Element " + CheckLoop + ". This is crtical!");
				Debug.LogError("Disabling Tag Dependant Sounds - falling back to default only, check log why!");
				UseTagDependantSounds = false;
			}
			if(TestTag == "Terrain") {
				if(UseTerrainFootsteps == true) {
					Debug.LogError("Dont add Terrain to tags if  Terrain Footsteps are enabled.");
					Debug.LogError("Disabling Terrain Footsteps - check log why!");
				UseTerrainFootsteps = false;
				}
			}
			CheckLoop += 1;
		}
		CheckLoop = 0;
	}
	if(UseTerrainFootsteps == true) {
		if(TerrainFootsteps.Length == 0) {
			Debug.LogError("Terrain Footsteps are on but no sounds are assigned.");
			Debug.LogError("Disabling Terrain Footsteps - check log why!");
		}
	}
}

function Footstep () {
	//Debug.Log("Footstepping");
	//PLAY THE FOOTSTEP ONLY IF THE CHARACTER CONTROLLER IS GROUNDED!
	//if(Controller.isGrounded) { //Fixed version ignores character controllers.
		if(UseTagDependantSounds == true) { //Use the tags only if there are any specified.
			if(UseDualLegMode == false) {
				for (var FootTag in FootstepTags) {
					//Debug.Log("Raycasting");
					if(Physics.Raycast(transform.position, RayDown, FootstepHit, LegLength)) {
						//Debug.Log("Raycasting" + FootstepHit.collider.tag);
						Debug.DrawRay(transform.position, RayDown, Color.green, 2);
						if(FootstepHit != null) {
							if(FootstepHit.collider.tag == FootTag) {
								if(Leg == 0) {
									GetComponent.<AudioSource>().Stop();
									GetComponent.<AudioSource>().PlayOneShot(Leg1Footsteps[Loop1]);
									Debug.Log("Playing sound!");
								}
								if(Leg == 1) {
									GetComponent.<AudioSource>().Stop();
									GetComponent.<AudioSource>().PlayOneShot(Leg2Footsteps[Loop1]);
									Debug.Log("Playing sound!");
								}
							} else {
								if(FootstepHit.collider.tag == "Untagged") {
									if(Leg == 0) {
										GetComponent.<AudioSource>().Stop();
										GetComponent.<AudioSource>().PlayOneShot(DefaultLeg1FootStep);
										//Debug.Log("Playing sound!");
									}
									if(Leg == 1) {
										GetComponent.<AudioSource>().Stop();
										GetComponent.<AudioSource>().PlayOneShot(DefaultLeg2FootStep);
										//Debug.Log("Playing sound!");
									}
								}
							}
							Loop1 += 1;
						}
					}
				}
				Loop1 = 0;
			}
			if(UseDualLegMode == true) {	//your legs aren´t infinitely long, check the distance of raycast so it doesnt
				if(Leg == 0) {				//use the footsteps for a surface below you.
					for (var FootTag1 in FootstepTags) {
						if(Physics.Raycast(FirstLeg.position, RayDown, FootstepHit, LegLength)) {
						//Debug.Log("Raycasting" + FootstepHit.collider.tag);
							Debug.DrawRay(FirstLeg.position, RayDown, Color.green, 2);
							if(FootstepHit != null) {
								if(FootstepHit.collider.tag == FootTag1) {
									if(Leg == 0) {
										GetComponent.<AudioSource>().Stop();
										GetComponent.<AudioSource>().PlayOneShot(Leg1Footsteps[Loop2]);
									//	Debug.Log("Playing sound!");
									}
								} else {
									if(FootstepHit.collider.tag == "Untagged") {
										if(Leg == 0) {
											GetComponent.<AudioSource>().Stop();
											GetComponent.<AudioSource>().PlayOneShot(DefaultLeg1FootStep);
											//Debug.Log("Playing sound!");
										}
									}
								}
							}
							Loop2 += 1;
						}
					}
					Loop2 = 0;
				}
				if(Leg == 1) {
					for (var FootTag2 in FootstepTags) {
						if(Physics.Raycast(SecondLeg.position, RayDown, FootstepHit, LegLength)) {
						//Debug.Log("Raycasting" + FootstepHit.collider.tag);
							Debug.DrawRay(SecondLeg.position, RayDown, Color.green, 2);
							if(FootstepHit != null) {
								if(FootstepHit.collider.tag == FootTag2) {
									if(Leg == 1) {
										GetComponent.<AudioSource>().Stop();
										GetComponent.<AudioSource>().PlayOneShot(Leg2Footsteps[Loop3]);
										//Debug.Log("Playing sound! " + Loop3);
									}
								} else {
									if(FootstepHit.collider.tag == "Untagged") {
										if(Leg == 1) {
											GetComponent.<AudioSource>().Stop();
											GetComponent.<AudioSource>().PlayOneShot(DefaultLeg2FootStep);
											//Debug.Log("Playing sound!");
										}
									}
								}
							}
							Loop3 += 1;
						}
					}
					Loop3 = 0;
				}
			}
			if(UseTerrainFootsteps == true) {
				if(Physics.Raycast(transform.position, RayDown, FootstepHit, LegLength)) {
					if(FootstepHit.collider.tag == "Terrain") {
						GetComponent.<AudioSource>().PlayOneShot(TerrainFootsteps[Random.Range(0, TerrainFootsteps.Length)]);
					}
				}
			}
		}
		if(UseTagDependantSounds == false) { //play the default footstep sound all the time.
			if(Leg == 0) {
				GetComponent.<AudioSource>().PlayOneShot(DefaultLeg1FootStep);
			}
			if(Leg == 1) {
				GetComponent.<AudioSource>().PlayOneShot(DefaultLeg2FootStep);
			}
		}
	//}
	//Switch Legs.
	CurStepLength = 0;
	if(Leg == 0) {
		Leg = 1;
	} else {
		Leg = 0;
	}
}