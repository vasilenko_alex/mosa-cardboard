1.0 - 1.1

Added Terrain Footsteps. For more information consult "1.0 - 1.1 Upgrade Guide.txt"

==============================================================================================

If you need help setting up or using this script consult the documentation PDF or e-mail me at
rudolfsagris@gmail.com

I'll be glad to help.

I hope you enjoy using this script as much as I enjoyed making it, thank you for purchasing :)