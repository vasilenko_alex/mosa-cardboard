﻿//Alright, me, this is production level code, not some toying-around stuff. Pls do comments where you can, k?

//Do not use Easy Footsteps 1.1 ever again. The code in that one is.. bad.
//This one is also much easier to use.

using UnityEngine;
using System.Collections;

public class EasyFootsteps2 : MonoBehaviour {

	AudioSource source;

	private float curSpeed = 0f;
	private Vector3 lastFramePos;

	Vector3 downPos;

	public float speed = 2f; //The speed during which footsteps will play at normal rate. Lower play slower. Higher play faster.
	float curProgress = 0; //1 causes a footstep to happen.

	public bool resetOnStop = true; //Resets curProgress on curSpeed < 0.01f, otherwise progress stays;

	public Transform Leg1;
	public Transform Leg2;
	public float raycastLength = 1; //Specifies how long are the leg raycasts.
	public LayerMask layers; //SPECIFIES WHAT LAYERS WILL THE RAYCAST COLLIDE WITH. IMPORTANT.
	//IF SOMETHING IS NOT WORKING, YOU WANT TO LOOK HEREREREREREREEREREREREREr.

	int curLeg = 1;

	public EFTag[] footsteps;

	public bool useJumpAndLandSounds = false;

	private bool grounded = true;
	private bool groundedLast = true;
	public Transform[] groundCheckPos;
	public float groundCheckLength = 0.1f;
	public LayerMask groundLayers;

	public AudioClip[] jumpSounds;
	public AudioClip[] landSounds;


	void Start () {

		//Get the position at start so it doesn't play a footstep on level start.
		lastFramePos = transform.position;

		//Cache the down position for much quicker access.
		downPos = transform.TransformDirection (Vector3.down);

		//Do the same with the audiosource.
		source = GetComponent<AudioSource>();

		if(source == null) {
			Debug.LogError ("Easy Footsteps 2 needs an audiosource!");
		}
		if(useJumpAndLandSounds) {
			if(groundCheckPos.Length <= 0) {
				Debug.LogError ("Disabling J&L sounds. No ground check positions specified.");
				useJumpAndLandSounds = false;
			}
		}
		if(Leg1 == null) {
			Debug.LogError ("Leg 1 is not assigned!");
			this.enabled = false;
		}
		if(Leg2 == null) {
			Debug.LogError ("Leg 2 is not assigned!");
			this.enabled = false;
		}
	}

	void FixedUpdate () {
		//Doing everything under fixedupdate because of EF skipping frames when game goes faster than physics (which is almost always)
		float difference = (transform.position - lastFramePos).magnitude;

		curSpeed = difference / speed / Time.fixedDeltaTime;

		//This is redundant, but I prefer to multiply with fixedDelta, in case you need curSpeed somewhere.
		curProgress += curSpeed * Time.fixedDeltaTime;

		if(useJumpAndLandSounds) {

			//This is what the Grounded value will be replaced
			bool isGrounded = false;

			//We check whether any of the ground points collide. Once one of them collides, others are not checked.
			//For performance reasons, of course.
			for(int i=0; i<groundCheckPos.Length; i++) {
				if(Physics.Raycast (groundCheckPos[i].position, downPos, groundCheckLength, groundLayers)) {
					isGrounded = true;
					break;
				}

			}

			grounded = isGrounded;

			//If from grounded to ungrounded - play the jump sound and vice versa.
			if(groundedLast == true) {
				if(grounded == false) {
					source.PlayOneShot (jumpSounds[Random.Range (0, jumpSounds.Length)]);
				}
			}

			if(groundedLast == false) {
				if(grounded == true) {
					source.PlayOneShot (landSounds[Random.Range (0, landSounds.Length)]);
				}
			}

			//And now we get which state we were in this frame.
			groundedLast = grounded;
		}

		//Here we control when to play the footstep.
		if(curProgress >= 1) {
			curProgress = 0;
			if(useJumpAndLandSounds) {
				if(grounded) {
					Footstep();
				}
			} else {
				Footstep();
			}
		}
		
		if(curSpeed < 0.01f) {
			if(resetOnStop) {
				curProgress = 0;
			}
		}

		lastFramePos = transform.position;
	}

	void Footstep () {
		//Raycast from the currently picked leg.
		RaycastHit efHit;
		if(curLeg == 1) {
			Physics.Raycast (Leg1.position, downPos, out efHit, raycastLength, layers);
			curLeg = 2;
		} else {
			Physics.Raycast (Leg2.position, downPos, out efHit, raycastLength, layers);
			curLeg = 1;
		}

		//Then read the raycasthit and play the correct footstep.
		if(efHit.collider != null) {
			foreach(EFTag tag in footsteps) {
				if(tag.tag == efHit.collider.tag) {
					//Warn the user if you he has forgotten to assign sounds.
					if(tag.sounds.Length > 0) {
						source.PlayOneShot (tag.sounds[Random.Range (0, tag.sounds.Length)]);
					} else {
						Debug.LogWarning ("Tag " + tag.tag + " is missing footstep sounds!");
					}
				}
			}
		}
	}
}


//This is a class called EFTag to make the inspector cleaner, and to make code cleaner as well.
//Using a class has literally no downsides as far as I can see.
[System.Serializable] //The serializable tag is mandatory to have it show up on the inspector.
public class EFTag {
	public AudioClip[] sounds;
	public string tag;

	public EFTag(AudioClip[] snd, string t) {
		sounds = snd;
		tag = t;
	}
}