﻿using UnityEngine;
using System.Collections;


public class MagnetSwitchForward : MonoBehaviour {
	public float Distance = 0.1f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Cardboard.SDK.Triggered) {
			
			
			transform.position = transform.position + Camera.main.transform.forward * Distance * Time.deltaTime;
			Vector3 newPosition = transform.position;
			newPosition += transform.forward * Time.deltaTime;
			transform.position = newPosition;
		}
	}
}



