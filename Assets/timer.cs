﻿using UnityEngine;
using System.Collections;

public class timer : MonoBehaviour {
	public float waitInSeconds = 1.0F;
	public float startTime;

	private Canvas myCanvas;
	// Use this for initialization
	void Start(){
		startTime = Time.time;
		myCanvas = GetComponent<Canvas>();
	}
	
	// Update is called once per frame
	void Update(){
		if(Time.time > startTime + waitInSeconds){
//			UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(gameObject, "Assets/timer.cs (16,4)", "Canvas");
			myCanvas.enabled = !myCanvas.enabled;
			Destroy (this);
		}
	}
}
